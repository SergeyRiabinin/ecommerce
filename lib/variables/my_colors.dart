

import 'package:flutter/material.dart';

class MyColors{
  static const background = Color(0xFFE5E5E5);

  static const mainOrange = Color(0xFFFF6E4E);
  static const mainDark = Color(0xFF010035);

  static const darkText = Color(0xFF000000);
  static const lightText = Color(0xFFFFFFFF);
  static const greyText = Color(0xFFCCCCCC);


  
}