import 'package:auto_size_text/auto_size_text.dart';
import 'package:ecommerce/variables/my_colors.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  
  int selectedCategory = 0;

  chooseCategory(int index) {
    selectedCategory = index;
    setState(() {});
  }

  List<String> allCategoryNames = [
    'Phones',
    'Computer',
    'Health',
    'Books',
    'Phones'
  ];

  List<String> allPrices = [
    '\$1,047',
    '\$300',
    '\$1,047',
    '\$300',
  ];

  List<String> allPreviousPrices = [
    '\$1,500',
    '\$400',
    '\$1,500',
    '\$400',
  ];

  List<String> allPreviewNames = [
    'Samsung Galaxy s20 Ultra',
    'Xiaomi Mi 10 Pro',
    'Samsung Note 20 Ultra',
    'Motorola One Edge ',
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.background,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: 12),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(width: 46),
                  Spacer(flex: 1),
                  Image.asset('assets/images/top_point.png',
                      fit: BoxFit.contain, height: 15, width: 15),
                  SizedBox(width: 11),
                  Text(
                    'Zihuatanejo, Gro',
                    textAlign: TextAlign.center,
                    style: GoogleFonts.roboto(
                      fontSize: 15,
                      fontWeight: FontWeight.w500,
                      color: MyColors.darkText,
                    ),
                    maxLines: 1,
                  ),
                  SizedBox(width: 5),
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
                    child: Image.asset(
                      'assets/images/arrow.png',
                      fit: BoxFit.contain,
                      height: 15,
                      width: 15,
                    ),
                  ),
                  Spacer(flex: 1),
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
                    child: Image.asset('assets/images/sort_icon.png',
                        fit: BoxFit.contain, height: 11, width: 13),
                  ),
                  SizedBox(width: 35),
                ],
              ),
              Container(
                margin: EdgeInsets.fromLTRB(17, 18, 33, 12),
                child: Row(
                  children: [
                    AutoSizeText(
                      'Select Category',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.roboto(
                        fontSize: 25,
                        fontWeight: FontWeight.w700,
                        color: MyColors.darkText,
                      ),
                      maxLines: 1,
                    ),
                    Spacer(flex: 1),
                    AutoSizeText(
                      'view all',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.roboto(
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                        color: MyColors.mainOrange,
                      ),
                      maxLines: 1,
                    ),
                  ],
                ),
              ),
              Container(
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      SizedBox(width: 12),
                      for (var i = 0; i < 5; i++)
                        GestureDetector(
                          onTap: () {
                            chooseCategory(i);
                          },
                          child: Column(
                            children: [
                              Container(
                                height: 71,
                                width: 71,
                                margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(40)),
                                  color: selectedCategory == i
                                      ? MyColors.mainOrange
                                      : Color(0xFFFFFFFF),
                                ),
                                alignment: Alignment.center,
                                child: Image.asset(
                                  'assets/images/category_icon$i.png',
                                  fit: BoxFit.contain,
                                  height: 40,
                                  width: 40,
                                  color: selectedCategory == i
                                      ? Color(0xFFFFFFFF)
                                      : Color(0xFFB3B3C3),
                                ),
                              ),
                              AutoSizeText(
                                allCategoryNames[i],
                                textAlign: TextAlign.center,
                                style: GoogleFonts.roboto(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w500,
                                  color: selectedCategory == i
                                      ? MyColors.mainOrange
                                      : MyColors.darkText,
                                ),
                                maxLines: 1,
                              ),
                            ],
                          ),
                        ),
                    ],
                  ),
                ),
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Container(
                        height: 34,
                        margin: EdgeInsets.fromLTRB(24, 24, 6, 8),
                        padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(17)),
                          color: Color(0xFFFFFFFF),
                        ),
                        child: Row(
                          children: [
                            Image.asset('assets/images/search_icon.png',
                                fit: BoxFit.contain, height: 20, width: 20),
                            searchTextField(),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 34,
                      width: 34,
                      margin: EdgeInsets.fromLTRB(6, 24, 37, 8),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(17)),
                        color: MyColors.mainOrange,
                      ),
                      alignment: Alignment.center,
                      child: Image.asset('assets/images/search_icon2.png',
                          fit: BoxFit.contain, height: 16, width: 16),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(17, 0, 33, 12),
                child: Row(
                  children: [
                    AutoSizeText(
                      'Hot sales',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.roboto(
                        fontSize: 25,
                        fontWeight: FontWeight.w700,
                        color: MyColors.darkText,
                      ),
                      maxLines: 1,
                    ),
                    Spacer(flex: 1),
                    AutoSizeText(
                      'see more',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.roboto(
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                        color: MyColors.mainOrange,
                      ),
                      maxLines: 1,
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(12, 12, 12, 12),
                child: Image.asset(
                  'assets/images/demo_slider.png',
                  fit: BoxFit.contain,
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(17, 6, 33, 12),
                child: Row(
                  children: [
                    AutoSizeText(
                      'Best Seller',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.roboto(
                        fontSize: 25,
                        fontWeight: FontWeight.w700,
                        color: MyColors.darkText,
                      ),
                      maxLines: 1,
                    ),
                    Spacer(flex: 1),
                    AutoSizeText(
                      'see more',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.roboto(
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                        color: MyColors.mainOrange,
                      ),
                      maxLines: 1,
                    ),
                  ],
                ),
              ),
              Row(
                children: [
                  SizedBox(width: 14),
                  card(0),
                  SizedBox(width: 11),
                  card(1),
                  SizedBox(width: 18),
                ],
              ),
              SizedBox(height: 10),
              Row(
                children: [
                  SizedBox(width: 14),
                  card(2),
                  SizedBox(width: 11),
                  card(3),
                  SizedBox(width: 18),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Expanded card(int index) {
    return Expanded(
      child: AspectRatio(
        aspectRatio: 181 / 227,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10.0),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              color: Color(0xFFFFFFFF),
            ),
            child: Stack(
              children: [
                Positioned(
                  top: 5,
                  left: 5,
                  right: 5,
                  child: Column(
                    children: [
                      Image.asset(
                        'assets/images/preview$index.png',
                        fit: BoxFit.contain,
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(8, 12, 0, 0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                AutoSizeText(
                                  allPrices[index],
                                  textAlign: TextAlign.left,
                                  style: GoogleFonts.roboto(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                    color: MyColors.darkText,
                                  ),
                                  maxLines: 1,
                                ),
                                SizedBox(width: 6),
                                AutoSizeText(
                                  allPreviousPrices[index],
                                  textAlign: TextAlign.left,
                                  style: GoogleFonts.roboto(
                                    fontSize: 10,
                                    fontWeight: FontWeight.w400,
                                    color: MyColors.greyText,
                                    decoration: TextDecoration.lineThrough,
                                  ),
                                  maxLines: 1,
                                ),
                              ],
                            ),
                            SizedBox(height: 4),
                            AutoSizeText(
                              allPreviewNames[index],
                              textAlign: TextAlign.left,
                              // overflow: TextOverflow.ellipsis,
                              style: GoogleFonts.roboto(
                                fontSize: 10,
                                fontWeight: FontWeight.w400,
                                color: MyColors.darkText,
                              ),
                              maxLines: 1,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: 11,
                  right: 15,
                  child: Container(
                    height: 25,
                    width: 25,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(13)),
                      color: Color(0xFFFFFFFF),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.3),
                          spreadRadius: 3,
                          blurRadius: 7,
                          offset: Offset(0, 0), // changes position of shadow
                        ),
                      ],
                    ),
                    alignment: Alignment.center,
                    child: Image.asset('assets/images/like0.png',
                        fit: BoxFit.contain, height: 10, width: 10),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Expanded searchTextField() {
    return Expanded(
      child: Container(
        padding: EdgeInsets.fromLTRB(12, 6, 12, 4),
        // width: w / 5,
        // color: Colors.yellow,
        alignment: Alignment.center,
        child: TextFormField(
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: 'Search',
            // contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
          ),
          maxLines: 1,
          keyboardAppearance: Brightness.values.first,
          cursorColor: Color(0xFFEC653E),
          style: GoogleFonts.roboto(
            fontSize: 12,
            fontWeight: FontWeight.w400,
            // color: Color(0xFFEC653E),
            height: 1.0,
          ),
        ),
      ),
    );
  }
}
